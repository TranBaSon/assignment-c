#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

typedef struct {
    char maSinhVien[10];
    char ten[10];
    char sdt[10];
} thongTinSinhVien;

thongTinSinhVien mangSinhVien[10];
int dem=0;
void removeStdChar(char string[]) {
    size_t length = strlen(string);
    while ((length > 0) && (string[length - 1] == '\n')) {
        --length;
        string[length] = '\0';
    }
}

void menu() {
    printf("-----> lựa chọn thứ bạn muốn <-----\n");
    printf("1. Thêm mới sinh viên.\n");
    printf("2. Hiển thị danh sách sinh viên.\n");
    printf("3. Lưu danh sách sinh viên từ file.\n");
    printf("4. Đọc danh sách sinh viên từ file.\n");
    printf("5. Thoát chương trình.\n");
}

void themMoiSinhVien(thongTinSinhVien mangSinhVien[],int i) {

    do {
        printf("Nhập mã sinh viên:\n");
        fgets(mangSinhVien[i].maSinhVien, sizeof(mangSinhVien[i].maSinhVien) * sizeof(char), stdin);
        removeStdChar(mangSinhVien[i].maSinhVien);

        if (strlen(mangSinhVien[i].maSinhVien) != 5) {
            printf("Nhập lại mã sinh viên (mã sinh viên phải đủ 5 kí tự).\n");
        }
    } while (strlen(mangSinhVien[i].maSinhVien) != 5);

    printf("Nhập tên sinh viên:\n");
    fgets(mangSinhVien[i].ten, sizeof(mangSinhVien[i].ten) * sizeof(char), stdin);
    removeStdChar(mangSinhVien[i].ten);

    printf("Nhập số điện thoaị sinh viên:\n");
    fgets(mangSinhVien[i].sdt, sizeof(mangSinhVien[i].sdt) * sizeof(char), stdin);
    removeStdChar(mangSinhVien[i].sdt);
    dem++;
    if (dem == 10) {
        printf("\n\n\n<-------->WARNING : danh sách sinh viên đã đầy.<-------->\n\n\n");
    }

    if (dem > 10) {
        exit(1);
    }
    }



void hienThiDanhSachSinhVien(thongTinSinhVien mangSinhVien[], int i) {

    printf("\n --------- Thông tin sinh viên ---------\n");
    printf("%-30s | %-30s | %-30s \n", "Ma Sinh Vien", "Tên", "Số Điện Thoại");

    for (int j = 0; j < 10; ++j) {

        printf(" %-30s| %-30s| %-30s \n", mangSinhVien[j].maSinhVien, mangSinhVien[j].ten, mangSinhVien[j].sdt);

    }

}

void luuThongTin(thongTinSinhVien mangSinhVien[], int i) {
    FILE *f1;
    f1 = fopen("../danhsachsinhvien.txt", "w");
    if (f1 != NULL) {

        fprintf(f1, "\n --------- Thông tin sinh viên --------\n");
        fprintf(f1, "%-30s | %-30s | %-30s \n", "Ma Sinh Vien", "Tên", "Số Điện Thoại");

        for (int j = 0; j < 10; ++j) {

            fprintf(f1, " %-30s| %-30s| %-30s \n", mangSinhVien[j].maSinhVien, mangSinhVien[j].ten,
                    mangSinhVien[j].sdt);
        }
        fclose(f1);
    }
    printf("Đã Lưu.\n");
}

void docDanhSach(char string[]) {
    FILE *f1;
    char kiTu[5000];

    f1 = fopen("../danhsachsinhvien.txt", "r");

    while (fgets(kiTu, 5000 * sizeof(char), f1) != NULL)
        printf("%s", kiTu);
    fclose(f1);
}


int main() {
    int luachon;
    char danhsach[255];
    while (1) {
        menu();
        scanf("%d", &luachon);
        getchar();
        switch (luachon) {
            case 1:
                    themMoiSinhVien(mangSinhVien, dem);
                break;
            case 2:
                hienThiDanhSachSinhVien(mangSinhVien, dem);
                break;
            case 3:
                luuThongTin(mangSinhVien, dem);
                break;
            case 4:
                docDanhSach(danhsach);
                break;
            case 5:
                return 0;
            default:
                printf("Sai cú pháp.\n");
                break;
        }
    }

}